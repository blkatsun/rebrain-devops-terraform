provider "vscale" {
  token = "${var.vscale_token}"
}
resource "vscale_ssh_key" "rebrain" {
 name   = "Rebrainme key"
 key    = "${var.ssh_key_rebrain}"
}
resource "vscale_ssh_key" "katsun" {
  name  = "Katsun key"
  key   = "${var.ssh_key_katsun}"
}
resource "vscale_scalet" "web" {
  name      = "katsun_vscale"
  make_from = "centos_7_64_001_master"
  rplan     = "small"
  location  = "spb0"
  ssh_keys  = ["${vscale_ssh_key.rebrain.id}","${vscale_ssh_key.katsun.id}"]
  
  provisioner "remote-exec" {
  connection {
   type        = "ssh"
   user        = "root"
   private_key = "${file("~/.ssh/id_rsa")}"
  }
   inline = [
      "echo -e \"${var.vscale_rpass}\n${var.vscale_rpass}\" | passwd root",
    ]
  } 
}
output "ip" {
  value = "${vscale_scalet.web.public_address}"
}
