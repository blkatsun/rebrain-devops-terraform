provider "vscale" {
  token = "${var.vscale_token}"
}
resource "vscale_ssh_key" "katsun" {
  name       = "Katsun key"
  key = "${var.ssh_key_katsun}"
}
resource "vscale_scalet" "web" {
  name   = "katsun_vscale"
  make_from = "centos_7_64_001_master"
  rplan = "small"
  location = "spb0"
  ssh_keys = [24042393,"${vscale_ssh_key.katsun.id}"]
}