provider "digitalocean" {
  token = "${var.do_token}"
}
resource "digitalocean_tag" "katsun" {
  name = "katsun"
}
# resource "digitalocean_ssh_key" "rebrain" {
#   name       = "Rebrain key"
#   public_key = "${var.ssh_key_rebrain}"
# }
resource "digitalocean_ssh_key" "katsun" {
  name       = "Katsun key"
  public_key = "${var.ssh_key_katsun}"
}
resource "digitalocean_droplet" "web" {
  image  = "ubuntu-18-04-x64"
  name   = "katsun-web"
  region = "nyc3"
  size   = "s-1vcpu-1gb"
  ssh_keys = [24042393,"${digitalocean_ssh_key.katsun.id}"]
  tags   = ["${digitalocean_tag.katsun.id}"]
}
