# Configure provider
provider "gitlab"{
    token = "${var.gitlab_token}"
}

# Add a project owned by the user
resource "gitlab_project" "rebrain-devops-terraform" {
    name = "rebrain-devops-terraform"
    visibility_level = "public"
    description = "DevOps repository"
    default_branch = "master"
}
# Add a deploy key to the project
resource "gitlab_deploy_key" "deploy_key" {
    project = "${gitlab_project.rebrain-devops-terraform.id}"
    title = "deploy key with push access"
    key = "${var.deploy_key}"
    can_push = true
}