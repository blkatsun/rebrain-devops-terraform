provider "vscale" {
  token = "${var.vscale_token}"
}
resource "vscale_ssh_key" "rebrain" {
 name       = "Rebrainme key"
 key = "${var.ssh_key_rebrain}"
}
resource "vscale_ssh_key" "katsun" {
  name       = "Katsun key"
  key = "${var.ssh_key_katsun}"
}
resource "vscale_scalet" "route53" {
  name   = "katsun_route53"
  make_from = "centos_7_64_001_master"
  rplan = "medium"
  location = "spb0"
  ssh_keys = ["${vscale_ssh_key.rebrain.id}","${vscale_ssh_key.katsun.id}"]
}
provider "aws" {
  access_key = "${var.aws_access_key}"
  secret_key = "${var.aws_secret_key}"
  region     = "us-east-1"
}

resource "aws_route53_record" "www" {
  zone_id = "${var.aws_zone_id}"
  name    = "blkatsun.devops.rebrain.srwx.net"
  type    = "A"
  ttl     = "300"
  records = ["${vscale_scalet.route53.public_address}"]
}