provider "vscale" {
  token = "${var.vscale_token}"
}
resource "vscale_ssh_key" "rebrain" {
 name   = "Rebrainme key"
 key    = "${var.ssh_key_rebrain}"
}
resource "vscale_ssh_key" "katsun" {
  name  = "Katsun key"
  key   = "${var.ssh_key_katsun}"
}
provider "aws" {
  access_key = "${var.aws_access_key}"
  secret_key = "${var.aws_secret_key}"
  region     = "us-east-1"
}
locals {
  devs_count = "${length(var.devs)}"
}
resource "random_string" "password" {
  count = "${local.devs_count}"
  length = 10
  special = true
  override_special = "/@\" "
  min_upper = 3
  min_lower = 4
}
locals {
  passwords = "${random_string.password.*.result}"
}
resource "vscale_scalet" "t9" {
  count = "${local.devs_count}"
  name   = "katsun_t9_${count.index}"
  make_from = "centos_7_64_001_master"
  rplan = "small"
  location = "spb0"
  ssh_keys = ["${vscale_ssh_key.rebrain.id}","${vscale_ssh_key.katsun.id}"]
  provisioner "remote-exec" {
    inline = [
      "hostnamectl set-hostname ${element(var.devs,count.index)}.devops.rebrain.srwx.net",
      "echo -e \"${element(local.passwords,count.index)}\n${element(local.passwords,count.index)}\" | passwd root",
    ]
  }  
}
locals {
  ips = ["${vscale_scalet.t9.*.public_address}"]
}
resource "aws_route53_record" "t9" {
  count = "${local.devs_count}"
  zone_id = "${var.aws_zone_id}"
  name    = "${element(var.devs,count.index)}.devops.rebrain.srwx.net"
  type    = "A"
  ttl     = "300"
  records = ["${element(local.ips, count.index)}"]
}
locals {
  routes = ["${aws_route53_record.t9.*.fqdn}"]
}
resource "null_resource" "export" {
  count = "${local.devs_count}"
  provisioner "local-exec" {
    command = "echo ${element(local.routes,count.index)} ${element(local.ips,count.index)} ${element(local.passwords,count.index)} >> ./devs.txt"
  }
}